<?php

/**
 * Autoload
 */
require 'vendor/autoload.php';

/**
 * Form Init
 */
$form = new ContactForm\Controller\Form();
$form->init();

//check if form has been submitted
$submitted      = !empty($_POST['submit']);
$invalid_fields = [];
$form_error_msg = '';

$template_file = 'public/view/form.php';

if ($submitted) {

    //first lets get our values.
    foreach (\ContactForm\Controller\Form::formFields() as $key => $input) {
        $value = $_POST[$key];

        //setting the values so they are displayed within the form still on failure.
        $contact_form[$key]['value'] = $value;

        if (!$input['validate']($value)) {
            $invalid_fields[] = $input['label'];
        }
    }

    //and validate ReCaptcha
    $token = $_POST['g-recaptcha-response'];

    if (!$form->formRecaptcha->validate($token)) {
        $invalid_fields[] = 'ReCaptcha';
        $form_error_msg   = 'Invalid ReCaptcha Submission. ';
    }

    //set an error message. This will display above the form.
    $form_error_msg .= 'Please revise the following parts: ' . implode(', ', $invalid_fields);

    //all fields were valid so we can insert into DB.
    if (empty($invalid_fields)) {

        $form->database->set_table('contact_submissions');

        $submission = [
            'name'      => $contact_form['name']['value'],
            'email'     => $contact_form['email']['value'],
            'telephone' => $contact_form['telephone']['value'],
            'message'   => $contact_form['message']['value'],
            'priority'  => getenv('PRIORITY_DEFAULT'),
            'status'    => getenv('STATUS_DEFAULT'),
            'browser'   => $_SERVER['HTTP_USER_AGENT'],
            'ip'        => $_SERVER['REMOTE_ADDR']
        ];

        //still need to modify the priority and status.

        $now                = new DateTime; //assumes the server is running on same timezone as the working hours.
        $start_of_day       = new DateTime("09:00");
        $end_of_day         = new DateTime("17:30");
        $same_day           = new DateTime("16:30");
        $submission['time'] = $now;

        //if its past 9am...
        if ($now > $start_of_day) {

            //if its before 17:30
            if ($now < $end_of_day) {

                //change the priority to morning reply if before 17:30
                $submission['priority'] = getenv('PRIORITY_MORNING_REPLY');

                //however, if before 16:30 then change to same day.
                if ($now < $same_day) {
                    $submission['priority'] = getenv('PRIORITY_SAME_DAY');
                }
            }
        }

        //now change the status based on the words and if contains URL.
        $message_words = str_word_count($submission['message']);
        $contains_url  = strpos($submission['message'], 'www.') !== false || preg_match('/http(s?):/', $submission['message']);

        if ($contains_url) {
            $submission['status'] = ($message_words > 20) ? getenv('STATUS_SPAM') : getenv('STATUS_POSSBILE_SPAM');
        } elseif ($message_words > 50) {
            $submission['status'] = getenv('STATUS_POSSIBLE_TEXT_SPAM');
        }

        //all good to insert into DB.
        //the mysql class prevents injection via prepared statements.
        //an exception will be thrown if the insert fails - no time to create error handler.
        $form->database->insert([
            'submission_status'    => $submission['status'],
            'submission_priority'  => $submission['priority'],
            'submission_ip'        => $submission['ip'],
            'submission_browser'   => $submission['browser'],
            'submission_name'      => $submission['name'],
            'submission_email'     => $submission['email'],
            'submission_telephone' => $submission['telephone'],
            'submission_message'   => $submission['message']
        ]);

        $template_file = 'public/view/success.php';
    }
}
$form->template->form_error_msg = $form_error_msg ?? '';
$form->template->submission     = $submission ?? null;

/**
 * Set the variables
 * Display the template
 */
$form->template->contact_form_temp = $contact_form ?? null;
$form->template->contact_form      = \ContactForm\Controller\Form::formFields();

$form->template->header = $form->template->render('public/view/header.php');
$form->template->footer = $form->template->render('public/view/footer.php');
echo $form->template->render($template_file);
