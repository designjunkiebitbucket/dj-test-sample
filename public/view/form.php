<?php echo $header ?? ''; ?>

    <form method="POST" id="contact-form">
        <?php if (!empty($form_error_msg)): ?>
            <div id="form-error"><?= $form_error_msg ?? '' ?></div>
        <?php endif ?>
        <div id="form-left">
            <?php if (!empty($contact_form)) {
                foreach ($contact_form as $key => $elem) { ?>
                    <span class="form-element">

                        <label for="<?= $key ?>"><?= $elem['label'] ?></label>
                        <?php if ($elem['type'] === 'textbox') { ?>
                            <textarea onblur="validate_form(event, '<?= $key ?>')" maxlength="300"
                                      placeholder="<?= $elem['placeholder'] ?>" length="50" name="<?= $key ?>"
                                      id="input-<?= $key ?>"><?= $contact_form_temp[$key]['value'] ?></textarea>
                            <?php
                        } else { ?>
                            <input onblur="validate_form(event, '<?= $key ?>')" type="<?= $elem['type'] ?>"
                                   name="<?= $key ?>"
                                   length="50" placeholder="<?= $elem['placeholder'] ?>" id="input-<?= $key ?>"
                                   value="<?= $contact_form_temp[$key]['value'] ?>"/>
                        <?php } ?>

                        <span class="error" id="error-<?= $key ?>"></span>
                    </span>
                    <?php
                }
            } ?>
        </div>
        <div id="form-right">
            <!-- recaptcha -->
            <div class="g-recaptcha" data-sitekey="<?= getenv('RC_SITE_KEY') ?>"></div>
            <input type="submit" name="submit" value="Send" onclick="validate_form(event, 'all')"/>
        </div>
    </form>

    <div style="clear:both"></div>
    <br><br>
    <a href="/view.php">Link to view submissions</a>

<?php echo $footer ?? ''; ?>