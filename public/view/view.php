<?php echo $header ?? ''; ?>
<?php
if (!empty($forms)) {
    foreach ($forms as $form) {
        /**
         * convert the priority and status.
         */
        $priority = [
                        getenv('PRIORITY_DEFAULT')       => 'Default',
                        getenv('PRIORITY_SAME_DAY')      => 'Same Day',
                        getenv('PRIORITY_MORNING_REPLY') => 'Morning Reply'][$form['submission_priority']];

        $status = [
                      getenv('STATUS_DEFAULT')            => 'Default',
                      getenv('STATUS_SPAM')               => 'Spam',
                      getenv('STATUS_POSSBILE_SPAM')      => 'Possible Spam',
                      getenv('STATUS_POSSIBLE_TEXT_SPAM') => 'Possible text spam'
                  ][$form['submission_status']];
        ?>

        <div class="row">
            <div class="row-left">
                <span>ID: <?= $form['submission_id'] ?></span>
                <span>IP: <?= $form['submission_ip'] ?></span>
                <span>Priority: <?= $priority ?></span>
                <span>Status: <?= $status ?></span>
                <span>Time: <?= $form['submission_timestamp'] ?></span>
            </div>
            <div class="row-right">
                <span><?= $form['submission_name'] ?></span>
                <span><?= $form['submission_email'] ?></span>
                <div><?= htmlspecialchars($form['submission_message']) ?></div>
                <span><?= $form['submission_browser'] ?></span>
            </div>
            <div style="clear:both"></div>
        </div>

        <?php
    }
}

?>

<?php echo $footer ?? ''; ?>

