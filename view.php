<?php

/**
 * Autoload
 */
require 'vendor/autoload.php';

/**
 * Form Init
 */
$form = new ContactForm\Controller\Form();
$form->init();

/**
 * Use the contact_submissions table
 */
$form->database->set_table('contact_submissions');

/**
 * Load the results
 */
$forms = $form->database->get();

/**
 * Set the variables
 * Display the template
 */
$form->template->forms = $forms;

$form->template->header = $form->template->render('public/view/header.php');
$form->template->footer = $form->template->render('public/view/footer.php');
echo $form->template->render('public/view/view.php');
