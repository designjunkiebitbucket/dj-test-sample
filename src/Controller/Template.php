<?php

namespace ContactForm\Controller;

class Template
{
    public $vars = array();

    /**
     * Get the variable
     *
     * @param $_var
     *
     * @return mixed
     */
    public function __get($_var)
    {
        return $this->vars[$_var];
    }

    /**
     * @param $_var
     * @param $_value
     *
     * @throws Exception
     */
    public function __set($_var, $_value)
    {
        if ($_var == 'view_template_file') {
            throw new Exception("Cannot bind variable named 'view_template_file'");
        }
        $this->vars[$_var] = $_value;
    }

    /**
     * @return array
     */
    public function _vars()
    {
        return $this->vars;
    }

    /**
     * @param $buf
     * @return mixed
     */
    function ob_html_compress($buf)
    {
        return preg_replace(
            ['/<!--(.*)-->/Uis', "/[[:blank:]]+/"], ['', ' '], str_replace(["\n", "\r", "\t"], '', $buf));
    }

    /**
     * @param $template_file
     *
     * @return false|string
     * @throws Exception
     */
    public function render($template_file)
    {
        if (array_key_exists('view_template_file', $this->vars)) {
            throw new Exception("Cannot bind variable called 'view_template_file'");
        }

        extract($this->vars);

        ob_start();
        if (file_exists($template_file) && !is_dir($template_file)):
            include($template_file);
            $load_content = ob_get_contents();

            ob_get_clean();

            return $load_content;
        else:
            return '';
        endif;
    }
}