<?php

namespace ContactForm\Controller;

use ContactForm\Config\FormConfig;
use ContactForm\Middleware\FormRecaptcha;
use ContactForm\Model\Database;

class Form
{
    public $database;
    public $template;
    public $formRecaptcha;

    public function init()
    {
        /**
         * Load the config
         */
        (new FormConfig())->load();

        $this->database      = new Database();
        $this->template      = new Template();
        $this->formRecaptcha = new FormRecaptcha();
    }

    /**
     * Return the current form data
     *
     * @return array[]
     */
    public static function formFields()
    {
        return [
            'name' => [
                'label'       => 'Name',
                'placeholder' => 'Enter your name',
                'id'          => 'name',
                'type'        => 'text',
                'value'       => '',
                'validate'    => function ($value) {
                    //only allow spaces and alpha characters. (although would need adjustment for foreign names)
                    return (bool)preg_match('/^[a-zA-Z\s]+$/', $value);
                }
            ],

            'email' => [
                'label'       => 'Email',
                'placeholder' => 'Enter your email address',
                'id'          => 'email',
                'type'        => 'email',
                'value'       => '',
                'validate'    => function ($value) {
                    return (bool)filter_var($value, FILTER_VALIDATE_EMAIL);
                }
            ],

            'telephone' => [
                'label'       => 'Telephone',
                'placeholder' => 'Enter your telephone number',
                'id'          => 'telephone',
                'type'        => 'number',
                'value'       => '',
                'validate'    => function ($value) {
                    //allow spaces and '+'
                    return (bool)preg_match('/^\+?[0-9\s]+$/', $value);
                }
            ],

            'message' => [
                'label'       => 'Message',
                'placeholder' => 'Enter your message',
                'id'          => 'message',
                'type'        => 'textbox',
                'value'       => '',
                'validate'    => function ($value) {
                    //max characters of 300 for our db.
                    return strlen($value) < 301 && strlen($value) > 5;
                }
            ],
        ];
    }
}