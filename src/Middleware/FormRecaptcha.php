<?php

namespace ContactForm\Middleware;

class FormRecaptcha
{
    private $recaptcha_url;

    public function __construct()
    {
        $this->recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
    }

    public function validate($token)
    {
        if (empty($token)) {
            return false;
        }

        try {
            //use cURL for POST request as is fairly standard.
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $this->recaptcha_url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, "response=$token&secret=" . getenv('RC_SECRET_KEY'));

            $response = curl_exec($curl);
            curl_close($curl);

            $json = json_decode($response);

            return $json->success;
        } catch (Exception $e) {
            throw new Exception('ReCaptcha Request failed.' . ' ' . $e->getMessage());
            return false;
        }

    }
}